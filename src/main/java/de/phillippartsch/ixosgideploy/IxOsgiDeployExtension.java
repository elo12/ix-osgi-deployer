/**
 * Copyright 2023 - Phillip Partsch <elo@phillipppartsch.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.phillippartsch.ixosgideploy;

import org.gradle.api.provider.Property;

/**
 * Configuration directive of gradle plugin
 */
public abstract class IxOsgiDeployExtension {
    /**
     * url of indexserver
     * @return url as string of indexserver, e.g. http://localhost:9090/ix-Archiv/ix
     */
    abstract public Property<String> getUrl();

    /**
     * username with main administrator permissions
     * @return username as string, e.g. Administrator
     */
    abstract public Property<String> getUsername();

    /**
     * password of username
     * @return password as string, e.g. elo
     */
    abstract public Property<String> getPassword();

    /**
     * name of the bundle. Must match the Bundle-SymbolicName property in the manifest
     * @return bundle name as string, e.g. de.phillippartsch.myfirstosgiplugin
     */
    abstract public Property<String> getBundleSymbolicName();
}
