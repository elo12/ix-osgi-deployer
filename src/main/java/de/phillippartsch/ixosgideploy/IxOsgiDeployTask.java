/**
 * Copyright 2023 - Phillip Partsch <elo@phillipppartsch.de>
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.phillippartsch.ixosgideploy;

import byps.RemoteException;
import de.elo.ix.client.IXConnFactory;
import de.elo.ix.client.IXConnection;
import org.gradle.api.DefaultTask;
import org.gradle.api.Task;
import org.gradle.api.UnknownTaskException;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.TaskAction;

import java.io.FileInputStream;
import java.nio.file.Paths;

/**
 * Gradle task implementation
 */
public abstract class IxOsgiDeployTask extends DefaultTask {

    @Input
    abstract public Property<String> getUrl();

    @Input
    abstract public Property<String> getUsername();

    @Input
    abstract public Property<String> getPassword();

    @Input
    abstract public Property<String> getBundleSymbolicName();

    @TaskAction
    public void deployOsgiBundle() {
        String url = getUrl().get();
        String username = getUsername().get();
        String password = getPassword().get();
        String bundleSymbolicName = getBundleSymbolicName().get();

        Task jarTask = getJarTask();
        if (jarTask == null) {
            throw new IllegalArgumentException("No jar task was found in the project. The ix-osgi-deploy plugin depends on jar task");
        }

        String jarTaskOutput = jarTask.getOutputs().getFiles().getAsPath();
        System.out.println("File=" + jarTaskOutput + " is present");
        if (!Paths.get(jarTaskOutput).toFile().exists()) {
            throw new IllegalArgumentException("Output=" + jarTaskOutput + " of jar task does not exist");
        }

        System.out.println("Prepare to deploy plugin=" + bundleSymbolicName + " to ix=" + url + " with user=" + username + " and password=****");
        IXConnection ixConnection;
        try {
            ixConnection = createIxConnection(url, username, password);
        } catch (RemoteException e) {
            System.err.println("Can not connect to ix " + e.getLocalizedMessage());
            throw new RuntimeException(e);
        }

        deployPlugin(jarTaskOutput, bundleSymbolicName, ixConnection);
        closeIxConnection(ixConnection);
    }

    /**
     * try to get a configured jar task
     *
     * @return jar task, null if no jar task is found
     */
    private Task getJarTask() {
        try {
            return getProject().getTasks().getByName("jar");
        } catch (UnknownTaskException e) {
            return null;
        }
    }

    /**
     * create a new connection to indexserver
     *
     * @param url      url of indexserver
     * @param username username with main administrator permissions
     * @param password password of the username
     * @return ix connection
     * @throws RemoteException if login fails
     */
    private IXConnection createIxConnection(String url, String username, String password) throws RemoteException {
        IXConnFactory ixConnFactory = new IXConnFactory(url, "de.pp.ix-osgi-deploy", "1.0");
        IXConnection ixConnection = ixConnFactory.create(username, password, "", null);

        System.out.println("Successfully connected to ix=" + url + " with user=" + ixConnection.getUserName() + " and password=****");
        return ixConnection;
    }

    /**
     * close ix connection
     *
     * @param ixConnection active ix connection
     */
    private void closeIxConnection(IXConnection ixConnection) {
        System.out.println("Close ix connection");
        ixConnection.close();
    }

    /**
     * deploy configured osgi bundle
     *
     * @param path               path to jar file on the filesystem
     * @param bundleSymbolicName name of the bundle
     * @param ixConnection       valid ix connection
     */
    private void deployPlugin(String path, String bundleSymbolicName, IXConnection ixConnection) {
        uninstallBundle(bundleSymbolicName, ixConnection);
        uploadBundle(path, bundleSymbolicName, ixConnection);
        startBundle(bundleSymbolicName, ixConnection);
    }

    /**
     * uninstall the plugin
     *
     * @param bundleSymbolicName name of the bundle
     * @param ixConnection       valid ix connection
     */
    private void uninstallBundle(String bundleSymbolicName, IXConnection ixConnection) {
        System.out.println("Uninstall plugin=" + bundleSymbolicName);
        try {
            long pluginId = ixConnection.getPluginService().getPlugin(bundleSymbolicName).getId();
            ixConnection.getPluginService().uninstall(pluginId);
        } catch (RemoteException e) {
            System.err.println("Can not uninstall plugin=" + bundleSymbolicName + ". This is not an error if plugin was not installed yet.");
        }
    }

    /**
     * upload the plugin
     *
     * @param path               path to jar file on the filesystem
     * @param bundleSymbolicName name of the bundle
     * @param ixConnection       valid ix connection
     */
    private void uploadBundle(String path, String bundleSymbolicName, IXConnection ixConnection) {
        System.out.println("Upload plugin=" + bundleSymbolicName + " from path=" + path);
        try (var fileStream = new FileInputStream(path)) {
            ixConnection.getPluginService().upload(fileStream);
        } catch (Exception e) {
            System.err.println("Can not install plugin=" + bundleSymbolicName);
            throw new RuntimeException(e);
        }
    }

    /**
     * try to start the plugin
     *
     * @param bundleSymbolicName name of the bundle
     * @param ixConnection       valid ix connection
     */
    private void startBundle(String bundleSymbolicName, IXConnection ixConnection) {
        System.out.println("Start plugin=" + bundleSymbolicName);
        try {
            long pluginId = ixConnection.getPluginService().getPlugin(bundleSymbolicName).getId();
            ixConnection.getPluginService().start(pluginId);
        } catch (RemoteException e) {
            System.err.println("Can not start plugin=" + bundleSymbolicName);
            throw new RuntimeException(e);
        }
    }
}
